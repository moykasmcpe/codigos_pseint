Proceso Capitulo_8_Mientras
	
	Definir i Como Entero;
	Definir Num Como Real;
	
	Escribir "DAME UN NUMERO";
	leer Num;
	
	Mientras i<50 Hacer
		i=i+1;
		Escribir Num ," x ",i, " = ", i*Num;
	Fin Mientras
	
FinProceso
